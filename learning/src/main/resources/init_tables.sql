USE sweater;
INSERT INTO phone (date, name, model, price) VALUES ('2011-05-01 15:12:12', 'Meizu', 's20', 2000.30),
                                                    ('2013-10-23 11:24:48', 'Xiaomi', 'Note 4', 1500.0),
                                                    ('2014-01-01 20:21:29', 'Meizu', 'Note 8', 5400.50),
                                                    ('2015-09-08 03:05:12', 'Xiaomi', 'Redmi 10', 6000.0),
                                                    ('2016-09-12 05:12:12', 'Samsung', 'Galaxy S', 7500.0);

INSERT INTO monitor (diagonal, date, name, price) VALUES (13.5, '2015-09-08 03:05:12', 'Acer', 1500.0),
                                                         (16.6, '2020-05-05 10:00:01', 'Philips', 1000.0),
                                                         (20.22, '2010-12-03 12:21:21', 'Panasonic', 2100.0),
                                                         (25.0, '2015-09-08 03:05:12', 'Sony', 3000.20),
                                                         (22.2, '2015-09-08 03:05:12', 'Sony', 2800.0);

INSERT INTO watch (color, date, name, price) VALUES ('green', '2016-09-12 05:12:12', 'Apple watch', 10000.0),
                                                    ('white', '2020-05-05 10:00:01', 'Xiaomi Mi Band 2', 2500.0),
                                                    ('black', '2015-09-08 03:05:12', 'Huaway band 3', 3000.0),
                                                    ('red', '2010-12-03 12:21:21', 'Xiaomi Mi Band 3', 2500.0),
                                                    ('black', '2011-05-01 15:12:12', 'Xiaomi Mi Band 4', 1000.0);
