package com.dzentech.repos;

import com.dzentech.domain.Watch;
import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface WatchRepo extends CrudRepository<Watch, Integer> {
    Optional<Watch> findById(Integer id);
    List<Watch> findByPriceBetweenAndInstantBetween(Double priceFrom, Double priceTo,
                                                    Instant dateFrom, Instant dateTo);
}
