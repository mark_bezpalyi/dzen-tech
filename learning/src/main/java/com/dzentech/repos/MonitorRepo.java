package com.dzentech.repos;

import com.dzentech.domain.Monitor;
import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface MonitorRepo extends CrudRepository<Monitor, Integer> {
    Optional<Monitor> findById(Integer id);
    List<Monitor> findByPriceBetweenAndInstantBetween(Double priceFrom, Double priceTo,
                                                    Instant dateFrom, Instant dateTo);
}
