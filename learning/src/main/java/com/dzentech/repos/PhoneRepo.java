package com.dzentech.repos;

import com.dzentech.domain.Phone;
import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface PhoneRepo extends CrudRepository<Phone, Integer> {
    List<Phone> findByName(String tag);
    Optional<Phone> findById(Integer id);
    List<Phone> findByPriceBetweenAndInstantBetween(Double priceFrom, Double priceTo,
                                                    Instant dateFrom, Instant dateTo);
}
