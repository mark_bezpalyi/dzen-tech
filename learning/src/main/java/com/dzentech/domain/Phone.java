package com.dzentech.domain;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Double price;

    private String model;

    @Column(name = "date", nullable = false, columnDefinition = "Timestamp")
    private Instant instant;

    public Phone() {
    }

    public Phone(String name, double price, Instant instant) {
        this.name = name;
        this.price = price;
        this.instant = instant;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
