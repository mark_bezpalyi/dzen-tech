package com.dzentech.domain;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Monitor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Double price;

    private Double diagonal;

    @Column(name = "date", nullable = false, columnDefinition = "Timestamp")
    private Instant instant;

    public Monitor() {
    }

    public Monitor(String name, Double price, Double diagonal, Instant instant) {
        this.name = name;
        this.price = price;
        this.diagonal = diagonal;
        this.instant = instant;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(Double diagonal) {
        this.diagonal = diagonal;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }
}
