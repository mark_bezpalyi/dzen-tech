package com.dzentech.domain;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Watch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Double price;

    private String color;

    @Column(name = "date", nullable = false, columnDefinition = "Timestamp")
    private Instant instant;

    public Watch() {
    }

    public Watch(String name, Double price, String color, Instant instant) {
        this.name = name;
        this.price = price;
        this.color = color;
        this.instant = instant;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }
}
