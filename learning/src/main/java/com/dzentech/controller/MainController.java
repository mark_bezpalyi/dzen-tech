package com.dzentech.controller;

import com.dzentech.domain.Monitor;
import com.dzentech.domain.Phone;
import com.dzentech.domain.User;
import com.dzentech.domain.Watch;
import com.dzentech.repos.MonitorRepo;
import com.dzentech.repos.PhoneRepo;
import com.dzentech.repos.WatchRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Controller
public class MainController {

    private Iterable<Phone> phoneList;

    private Iterable<Monitor> monitorList;

    private Iterable<Watch> watchList;

    private final EditDomains editDomains = new EditDomains();

    @Autowired
    private PhoneRepo phoneRepo;

    @Autowired
    private MonitorRepo monitorRepo;

    @Autowired
    private WatchRepo watchRepo;

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {

        return "greeting";
    }

    @PostMapping("/add")
    public String add(
            @AuthenticationPrincipal User user,
            @RequestParam String text,
            Map<String, Object> model) {
        if (text != null && text.equals("phones")) {
            phoneList = phoneRepo.findAll();
        } else if (text != null && text.equals("monitors")) {
            monitorList = monitorRepo.findAll();
        } else if (text != null && text.equals("watches")) {
            watchList = watchRepo.findAll();
        }
        updateLists(model);
        return "main";
    }

    @PostMapping("/remove")
    public String remove(@AuthenticationPrincipal User user,
                         @RequestParam String text,
                         Map<String, Object> model) {
        if (text != null && text.equals("phones")) {
            phoneList = null;
        } else if (text != null && text.equals("monitors")) {
            monitorList = null;
        } else if (text != null && text.equals("watches")) {
            watchList = null;
        }
        updateLists(model);
        return "main";
    }

    @PostMapping("/deletePhone")
    public String deletePhone(@AuthenticationPrincipal User user,
                              @RequestParam Integer number,
                              @RequestParam(value = "submission", required = false) String checkboxValue,
                              Map<String, Object> model) {
        List<Phone> newModel = new ArrayList<>();
        if (checkboxValue != null && checkboxValue.equals("on") && phoneList != null) {
            boolean isAvailable = false;
            for (Phone phone : phoneList) {
                if (phone.getId().equals(number)) {
                    isAvailable = true;
                    break;
                }
            }
            if (!isAvailable) {
                updateLists(model);
                return "main";
            }
            try {
                phoneRepo.deleteById(number);
            } catch (EmptyResultDataAccessException e) {
                updateLists(model);
                return "main";
            }
            for (Phone phone : phoneList) {
                if (!phone.getId().equals(number)) {
                    newModel.add(phone);
                }
            }
            phoneList = newModel;
        }
        updateLists(model);
        return "main";
    }

    @PostMapping("/deleteMonitor")
    public String deleteMonitor(@AuthenticationPrincipal User user,
                                @RequestParam Integer number,
                                @RequestParam(value = "submission", required = false) String checkboxValue,
                                Map<String, Object> model) {
        List<Monitor> newModel = new ArrayList<>();
        if (checkboxValue != null && checkboxValue.equals("on") && monitorList != null) {
            boolean isAvailable = false;
            for (Monitor monitor : monitorList) {
                if (monitor.getId().equals(number)) {
                    isAvailable = true;
                    break;
                }
            }
            if (!isAvailable) {
                updateLists(model);
                return "main";
            }
            try {
                monitorRepo.deleteById(number);
            } catch (EmptyResultDataAccessException e) {
                updateLists(model);
                return "main";
            }
            for (Monitor monitor : monitorList) {
                if (!monitor.getId().equals(number)) {
                    newModel.add(monitor);
                }
            }
            monitorList = newModel;
        }
        updateLists(model);
        return "main";
    }

    @PostMapping("/deleteWatch")
    public String deleteWatch(@AuthenticationPrincipal User user,
                              @RequestParam Integer number,
                              @RequestParam(value = "submission", required = false) String checkboxValue,
                              Map<String, Object> model) {
        List<Watch> newModel = new ArrayList<>();
        if (checkboxValue != null && checkboxValue.equals("on") && watchList != null) {
            boolean isAvailable = false;
            for (Watch watch : watchList) {
                if (watch.getId().equals(number)) {
                    isAvailable = true;
                    break;
                }
            }
            if (!isAvailable) {
                updateLists(model);
                return "main";
            }
            try {
                watchRepo.deleteById(number);
            } catch (EmptyResultDataAccessException e) {
                updateLists(model);
                return "main";
            }
            for (Watch watch : watchList) {
                if (!watch.getId().equals(number)) {
                    newModel.add(watch);
                }
            }
            watchList = newModel;
        }
        updateLists(model);
        return "main";
    }

    private void updateLists(Map<String, Object> model) {
        if (phoneList != null) {
            model.put("phones", phoneList);
        }

        if (monitorList != null) {
            model.put("monitors", monitorList);
        }

        if (watchList != null) {
            model.put("watches", watchList);
        }
    }

    @PostMapping("addAll")
    public String addAll(@AuthenticationPrincipal User user,
                         Map<String, Object> model) {
        phoneList = phoneRepo.findAll();
        monitorList = monitorRepo.findAll();
        watchList = watchRepo.findAll();
        updateLists(model);
        return "main";
    }

    @PostMapping("removeAll")
    public String removeAll(@AuthenticationPrincipal User user,
                            Map<String, Object> model) {
        phoneList = null;
        monitorList = null;
        watchList = null;
        updateLists(model);
        return "main";
    }

    @GetMapping("/main")
    public String main(@RequestParam(required = false) Double priceFromFilter,
                       @RequestParam(required = false) Double priceToFilter,
                       @RequestParam(required = false, defaultValue = "2000-10-20") String dateFromFilterString,
                       @RequestParam(required = false, defaultValue = "2021-10-20") String dateToFilterString,
                       Model model) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateFromFilter;
        try {
            dateFromFilter = LocalDate.parse(dateFromFilterString, formatter);
        } catch (DateTimeParseException e) {
            dateFromFilter = null;
        }
        LocalDate dateToFilter;
        try {
            dateToFilter = LocalDate.parse(dateToFilterString, formatter);
        } catch (DateTimeParseException e) {
            dateToFilter = null;
        }

        Iterable<Phone> phones;
        Iterable<Monitor> monitors;
        Iterable<Watch> watches;

        if (priceFromFilter == null || priceFromFilter <= 0) {
            priceFromFilter = 0.0;
        }
        if (priceToFilter == null || priceToFilter <= 0) {
            priceToFilter = 2_000_000.00;
        }
        if (dateFromFilter == null || dateFromFilter.isBefore(LocalDate.of(2000, 10, 10))) {
            dateFromFilter = LocalDate.of(2000, 10, 10);
        }
        if (dateToFilter == null || dateToFilter.isBefore(LocalDate.of(2000, 10, 10))) {
            dateToFilter = LocalDate.of(2021, 1, 1);
        }

        if (phoneList != null) {
            phones = phoneRepo.findByPriceBetweenAndInstantBetween(priceFromFilter, priceToFilter,
                    dateFromFilter.atStartOfDay(ZoneId.systemDefault()).toInstant(),
                    dateToFilter.atStartOfDay(ZoneId.systemDefault()).toInstant());
            phoneList = phones;
            model.addAttribute("phones", phones);
        }
        if (monitorList != null) {
            monitors = monitorRepo.findByPriceBetweenAndInstantBetween(priceFromFilter, priceToFilter,
                    dateFromFilter.atStartOfDay(ZoneId.systemDefault()).toInstant(),
                    dateToFilter.atStartOfDay(ZoneId.systemDefault()).toInstant());
            monitorList = monitors;
            model.addAttribute("monitors", monitors);
        }
        if (watchList != null) {
            watches = watchRepo.findByPriceBetweenAndInstantBetween(priceFromFilter, priceToFilter,
                    dateFromFilter.atStartOfDay(ZoneId.systemDefault()).toInstant(),
                    dateToFilter.atStartOfDay(ZoneId.systemDefault()).toInstant());
            watchList = watches;
            model.addAttribute("watches", watches);
        }
        return "main";
    }

    @PostMapping("/editMonitor")
    public String editMonitor(
            @RequestParam Integer number,
            @RequestParam String name,
            @RequestParam Double diagonal,
            @RequestParam Double price,
            @RequestParam String dateString,
            Map<String, Object> model) {
        if(monitorList == null) {
            updateLists(model);
            return "main";
        }
        boolean isAvailable = false;
        for(Monitor monitor : monitorList) {
            if(monitor.getId().equals(number)) {
                isAvailable = true;
                break;
            }
        }
        if(isAvailable) {
            monitorList = editDomains.editMonitor(monitorList, monitorRepo, number, name, diagonal, price, dateString);
        }
        updateLists(model);
        return "main";
    }

    @PostMapping("/editPhone")
    public String editPhone(
            @RequestParam Integer number,
            @RequestParam String name,
            @RequestParam String phModel,
            @RequestParam Double price,
            @RequestParam String dateString,
            Map<String, Object> model) {
        if(phoneList == null) {
            updateLists(model);
            return "main";
        }
        boolean isAvailable = false;
        for(Phone phone : phoneList) {
            if(phone.getId().equals(number)) {
                isAvailable = true;
                break;
            }
        }
        if(isAvailable) {
            phoneList = editDomains.editPhone(phoneList, phoneRepo, number, name, phModel, price, dateString);
        }
        updateLists(model);
        return "main";
    }

    @PostMapping("/editWatch")
    public String editWatch(
            @RequestParam Integer number,
            @RequestParam String name,
            @RequestParam String color,
            @RequestParam Double price,
            @RequestParam String dateString,
            Map<String, Object> model) {
        if(watchList == null) {
            updateLists(model);
            return "main";
        }
        boolean isAvailable = false;
        for(Watch watch : watchList) {
            if(watch.getId().equals(number)) {
                isAvailable = true;
                break;
            }
        }
        if(isAvailable) {
            watchList = editDomains.editWatch(watchList, watchRepo, number, name, color, price, dateString);
        }
        updateLists(model);
        return "main";
    }
}
