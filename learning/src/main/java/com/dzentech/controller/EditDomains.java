package com.dzentech.controller;

import com.dzentech.domain.Monitor;
import com.dzentech.domain.Phone;
import com.dzentech.domain.Watch;
import com.dzentech.repos.MonitorRepo;
import com.dzentech.repos.PhoneRepo;
import com.dzentech.repos.WatchRepo;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

public class EditDomains {

    public Iterable<Monitor> editMonitor(Iterable<Monitor> monitorList,
                                         MonitorRepo monitorRepo,
                                         Integer number,
                                         String name,
                                         Double diagonal,
                                         Double price,
                                         String dateString) {
        Optional<Monitor> byId = monitorRepo.findById(number);
        if (byId.isEmpty()) {
            return monitorList;
        }
        Monitor monitor = byId.get();
        if (name != null && !name.isEmpty()) {
            monitor.setName(name);
        }

        if (diagonal != null && diagonal > 10) {
            monitor.setDiagonal(diagonal);
        }

        if (price != null && price > 0.0) {
            monitor.setPrice(price);
        }

        if (dateString != null && !dateString.isEmpty()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date;
            try {
                date = LocalDate.parse(dateString, formatter);
            } catch (DateTimeParseException e) {
                date = null;
            }
            if (date != null) {
                monitor.setInstant(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
            }
        }

        monitorRepo.save(monitor);
        for (Monitor monitor1 : monitorList) {
            if (monitor1.getId().equals(number)) {
                monitor1.setName(monitor.getName());
                monitor1.setDiagonal(monitor.getDiagonal());
                monitor1.setPrice(monitor.getPrice());
                monitor1.setInstant(monitor.getInstant());
                break;
            }
        }
        return monitorList;
    }

    public Iterable<Phone> editPhone(Iterable<Phone> phoneList,
                                     PhoneRepo phoneRepo,
                                     Integer number,
                                     String name,
                                     String phModel,
                                     Double price,
                                     String dateString) {
        Optional<Phone> byId = phoneRepo.findById(number);
        if (byId.isEmpty()) {
            return phoneList;
        }
        Phone phone = byId.get();
        if (name != null && !name.isEmpty()) {
            phone.setName(name);
        }

        if (phModel != null && !phModel.isEmpty()) {
            phone.setModel(phModel);
        }

        if (price != null && price > 0) {
            phone.setPrice(price);
        }

        if (dateString != null && !dateString.isEmpty()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date;
            try {
                date = LocalDate.parse(dateString, formatter);
            } catch (DateTimeParseException e) {
                date = null;
            }
            if (date != null) {
                phone.setInstant(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
            }
        }
        phoneRepo.save(phone);
        for (Phone phone1 : phoneList) {
            if (phone1.getId().equals(number)) {
                phone1.setName(phone.getName());
                phone1.setModel(phone.getModel());
                phone1.setPrice(phone.getPrice());
                phone1.setInstant(phone.getInstant());
                break;
            }
        }
        return phoneList;
    }

    public Iterable<Watch> editWatch(
            Iterable<Watch> watchList,
            WatchRepo watchRepo,
            Integer number,
            String name,
            String color,
            Double price,
            String dateString) {
        Optional<Watch> byId = watchRepo.findById(number);
        if (byId.isEmpty()) {
            return watchList;
        }
        Watch watch = byId.get();
        if (name != null && !name.isEmpty()) {
            watch.setName(name);
        }

        if (color != null && !color.isEmpty()) {
            watch.setColor(color);
        }

        if (price != null && price > 0) {
            watch.setPrice(price);
        }

        if (dateString != null && !dateString.isEmpty()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date;
            try {
                date = LocalDate.parse(dateString, formatter);
            } catch (DateTimeParseException e) {
                date = null;
            }
            if (date != null) {
                watch.setInstant(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
            }
        }
        watchRepo.save(watch);
        for (Watch watch1 : watchList) {
            if (watch1.getId().equals(number)) {
                watch1.setName(watch.getName());
                watch1.setColor(watch.getColor());
                watch1.setPrice(watch.getPrice());
                watch1.setInstant(watch.getInstant());
                break;
            }
        }
        return watchList;
    }
}
